import { defineStore } from 'pinia'

export const useInstrumentStore =  defineStore(
    "instruments",
    {
        state: () => ({
            instruments: [
                {
                    image: "https://livelouder.co.uk/wp-content/uploads/2022/10/Ibanez-TOD10N-TKF-Time-Henson-Nylon-Guitar-1-768x768.jpg",
                    name: "TOD 10N",
                    type: "Guitare",
                    price: 699
                },
                {
                    image: "https://www.pianogallery.com/wp-content/uploads/2019/08/cvp-809gp_colors_b_735x735_2351864f52aaab235f9a86bdc87fa540.jpg",
                    name: "Yamaha CVP-809GP",
                    type: "Piano",
                    price: 18499.99
                }
            ]
        }),
        getters: {
            getByName: (state) => {
                return (name) => state.instruments.find((i) => i.name === name)
            },
        }
    }
)
